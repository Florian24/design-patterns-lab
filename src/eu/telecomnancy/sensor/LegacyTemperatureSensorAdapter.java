package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorAdapter extends TemperatureSensor implements ISensor{

	private LegacyTemperatureSensor Sensor;
	
	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor sensor)
	{
		this.Sensor = sensor;
	}
	
	@Override
	public void on() {
		boolean status;
		status =  Sensor.getStatus();
		if (status == false)
		{
			Sensor.onOff();
		}
	}

	@Override
	public void off() 
	{
		boolean status;
		status =  Sensor.getStatus();
		if (status == true)
		{
			Sensor.onOff();
		}
	}

	@Override
	public boolean getStatus()
	{
		return Sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (Sensor.getStatus() == true)
		{
			Sensor.getTemperature();
            setChanged(); 
            notifyObservers(this);//On signal � l'observer que les donn�es on chang�es
		}
		else throw new SensorNotActivatedException("Sensor must be activated !"); 
		}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		if (Sensor.getStatus() == true)
		{		
			return Sensor.getTemperature();
		}
		else throw new SensorNotActivatedException("Sensor must be activated !"); 
		}

}
