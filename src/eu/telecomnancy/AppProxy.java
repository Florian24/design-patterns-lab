package eu.telecomnancy;

import eu.telecomnancy.patterns.proxy.Proxy;

import eu.telecomnancy.ui.ConsoleUI;

public class AppProxy {

	public static void main(String[] args) {
        
    	Proxy proxy = new Proxy();
    	new ConsoleUI(proxy);
    }
	
}
