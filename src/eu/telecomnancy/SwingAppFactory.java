package eu.telecomnancy;

import eu.telecomnancy.patterns.factory.FactorySensor;
import eu.telecomnancy.patterns.proxy.Proxy;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppFactory {

    public static void main(String[] args) {
       
    	ISensor sensor;
    	FactorySensor MaFactory = new FactorySensor();
    	sensor = MaFactory.Factory("temp");
        new MainWindow(sensor);
    }
	
}
