package eu.telecomnancy.patterns.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

//Capteur off
public class StateOff extends State{

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Capteur �teind");
	}

	@Override
	public void setValue(double value) throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Capteur �teind");
	}

	@Override
	public void on(Context c) {
		c.on();
	}

	@Override
	public void off(Context c) {
		System.out.println("Capteur deja �teind");
	}

}
