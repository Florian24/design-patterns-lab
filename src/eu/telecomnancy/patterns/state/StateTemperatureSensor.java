package eu.telecomnancy.patterns.state;

import java.util.Observable;
import java.util.Random;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateTemperatureSensor extends Observable implements ISensor {
    Context context = new Context();
    double value = 0;

    @Override
    public void on() {
        this.context.on();
    }

    @Override
    public void off() {
    	this.context.off();
    }

    @Override
    public boolean getStatus() {
        return this.context.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        this.context.setValue((new Random()).nextDouble() * 100);
        setChanged(); 
        notifyObservers(this);//On notifie a l'observeur la modification des donn�es
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	return this.context.getValue();
    }
}
