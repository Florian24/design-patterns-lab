package eu.telecomnancy.patterns.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

//capteur ON
public class StateOn extends State{

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.value;
	}

	@Override
	public void setValue(double value) throws SensorNotActivatedException {
		this.value = value;
	}

	@Override
	public void on(Context c) {
		System.out.println("Capteur deja actif");
	}

	@Override
	public void off(Context c) {
		c.off();
	}

}
