package eu.telecomnancy.patterns;

import java.util.Observable;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class AdapterLegacyTemperatureSensor extends Observable implements ISensor {

	private LegacyTemperatureSensor adaptee;
	
	public AdapterLegacyTemperatureSensor(LegacyTemperatureSensor adaptee) {
		this.adaptee = adaptee;
	}

	@Override
	public void on() {
		if(adaptee.getStatus() == false)
			adaptee.onOff();
	}

	@Override
	public void off() {
		if(adaptee.getStatus() == true)
			adaptee.onOff();
	}

	@Override
	public boolean getStatus() {
		return adaptee.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!adaptee.getStatus())
			throw new SensorNotActivatedException("Sensor not activated");
		adaptee.getTemperature(); //mise � jour de la valeur
		setChanged();
		notifyObservers(this); //On signal � l'observer que les donn�es on chang�es
		return;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!adaptee.getStatus())
			throw new SensorNotActivatedException("Sensor not activated");
		return adaptee.getTemperature();
	}

}
