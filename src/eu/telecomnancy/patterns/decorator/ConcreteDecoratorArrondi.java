package eu.telecomnancy.patterns.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class ConcreteDecoratorArrondi extends Decorator {

	public ConcreteDecoratorArrondi(ISensor decorated) {
		super(decorated);
	}

	public double getValue() throws SensorNotActivatedException {
		System.out.println("Temperature arrondie :");
		return Math.round(this.decorated.getValue());
	}

}
