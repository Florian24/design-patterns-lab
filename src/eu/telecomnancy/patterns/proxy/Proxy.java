package eu.telecomnancy.patterns.proxy;

import java.util.Observable;
import java.util.Observer;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class Proxy extends Observable implements ISensor{

	ISensor sensor;
	
	public Proxy() {
		this.sensor = new TemperatureSensor();
	}

	@Override
	public synchronized void addObserver(Observer o) {
		((Observable)this.sensor).addObserver(o);
	}

	@Override
	public void on() {
		System.out.println( new java.util.Date() +" on()");
		this.sensor.on();
	}

	@Override
	public void off() {
		System.out.println(new java.util.Date() +" off()");
		this.sensor.off();
	}

	@Override
	public boolean getStatus() {
		boolean status = this.sensor.getStatus();
		System.out.println(new java.util.Date() +" getStatus() "+ status);	
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.sensor.update();
		System.out.println(new java.util.Date() +" update()");	
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		double value = this.sensor.getValue();
		System.out.println(new java.util.Date() +" getValue() "+value);	
		return value;
	}

}
