package eu.telecomnancy.patterns.factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensorAdapter;
import eu.telecomnancy.sensor.TemperatureSensor;

public class FactorySensor {
	
	
	
public	ISensor Factory(String typeSensor)
	{
		if (typeSensor == "temp" )
		{
		 return new TemperatureSensor();
		}
		
		else if (typeSensor == "Legacy")
		{
	       LegacyTemperatureSensor s = new LegacyTemperatureSensor();      
	       return new LegacyTemperatureSensorAdapter(s);
		}
		else return null;
		
	}

}
