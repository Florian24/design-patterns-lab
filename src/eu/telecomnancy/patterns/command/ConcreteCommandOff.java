package eu.telecomnancy.patterns.command;

import eu.telecomnancy.sensor.ISensor;

public class ConcreteCommandOff implements Command{

	ISensor reicever;

	public ConcreteCommandOff(ISensor reicever) {
		this.reicever = reicever;
	}
	
	@Override
	public Object execute() throws Exception {
		reicever.off();
		return null;
	}	
	
}
