package eu.telecomnancy.patterns.command;

import eu.telecomnancy.sensor.ISensor;

public class ConcreteCommandUpdate implements Command{

	ISensor reicever;

	public ConcreteCommandUpdate(ISensor reicever) {
		this.reicever = reicever;
	}
	
	@Override
	public Object execute() throws Exception {
		reicever.update();
		return null;
	}	
	
}
