package eu.telecomnancy.patterns.command;

public interface Command {

	public Object execute() throws Exception;
}
