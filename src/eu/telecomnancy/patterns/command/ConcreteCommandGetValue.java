package eu.telecomnancy.patterns.command;

import eu.telecomnancy.sensor.ISensor;

public class ConcreteCommandGetValue implements Command{

	ISensor reicever;

	public ConcreteCommandGetValue(ISensor reicever) {
		this.reicever = reicever;
	}
	
	@Override
	public Object execute() throws Exception {
		return reicever.getValue();
	}	
	
}
