package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensorAdapter;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppAdapter {

    public static void main(String[] args){
    	
        LegacyTemperatureSensor LegacySensor = new LegacyTemperatureSensor();
        ISensor sensor = new LegacyTemperatureSensorAdapter(LegacySensor); 	
        new MainWindow(sensor);
    }

}
